#coding: utf-8
from json import dumps as json
from odoo import http
from odoo.addons.web.controllers import main
from odoo import SUPERUSER_ID
from odoo.addons.web.controllers.main import serialize_exception
from random import choice


class OdooApi(http.Controller):

    @http.route('/api', type='http', auth='none')
    def form_login(self):
        return http.request.render('sll_api.api_login')

    @http.route('/api/auth', type='http',
                auth='none', methods=['POST'], csrf=False)
    @serialize_exception
    def api_login(self, **post):
        content = {}
        print('meooooooooooo')
        print("code go here")
        print(post)

        if post.get('login') and post.get('pw'):
            session = http.request.session
            print(session)

            #request mot session moi voi tham so DB,
            # login, password
            login = http.request.session.authenticate(
                session['db'], post['login'], post['pw'])

            #lay du lieu tu cac API duoc authenticate


            print(login)
            if not login:
                content['status'] = 'denied'
                return http.request.make_response(
                    json(content), [('Access-Control-Allow-Origin', '*')])
            users = http.request.env['res.users'].sudo().search([
                ('login', '=', post['login'])])
            if users:
                for user in users:
                    if not user['token']:
                        uid_ = user['id']
                        token = self.get_token(uid_)
                    content = {
                        'name': user['name'] or '',
                        'login': post['login'] or '',
                        'token': user['token'] or token,
                    }
            if not content:
                content = {'status': 'error'}
            return http.request.make_response(json(content), [
                ('Access-Control-Allow-Origin', '*')])

    def get_token(self, uid_):
        length = 25
        char = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
        token = ''.join([choice(char) for i in xrange(length)])
        check_token = http.request.env['res.users'].search(
            [('token', '=', token)])
        while check_token:
            token = ''.join([choice(char) for i in xrange(length)])
            check_token = http.request.env['res.users'].search(
                [('token', '=', token)])
        res = http.request.env['res.users'].browse(uid_).sudo().write({'token': token})
        return token

    @http.route('/api/student', type='http',
                auth='user', methods=['POST'], csrf=False)
    def get_student(self, **kwa):
        content = {}
        if kwa.get('token'):
            check_token = http.request.env['res.users'].search([(
                'token', '=', kwa.get('token'))])
            if check_token:
                uid_ = check_token.id
                students = http.request.env['bms.student.management'].sudo(
                    uid_).search([('user_id', '=', uid_)])
                if uid_ == SUPERUSER_ID:
                    students = http.request.env['bms.student.management'].sudo(
                    uid_).search([])
                if students:
                    for s in students:
                        content[s['name']] = {
                            'student_code': s.student_code or '',
                            'gender': s.gender or '',
                            'birthday': s.birth_day or '',
                            'class': s.study_class_id.name or '',
                            'home_teacher': s.study_class_id.homeroom_teacher.name or '',
                            'parent': s.parent_id.name or '',
                        }
                return http.request.make_response(json(content), [
                ('Access-Control-Allow-Origin', '*')])

    @http.route('/api/class_teacher', type='http', auth='none', methods=['POST'], csrf=False)
    def class_teacher(self, **kwa):
        content = {}
        if kwa.get('token'):
            check_token = http.request.env['res.users'].search([(
                'token', '=', kwa.get('token'))])
            if check_token:
                try:
                    uid_ = check_token.id
                    classs = http.request.env['bms.study.class'].sudo(
                        uid_).search([])
                    if classs:
                        for s in classs :
                            content[s['name']] = {
                                'key' : s.id,
                                'school_id': s.school_id.name,
                                'group_of_class_id': s.group_of_class_id.name,
                                'sehoot_year_id': s.sehoot_year_id.name,
                                'student_numbers': s.student_numbers,
                                'class_room_number': s.class_room_number,
                            }
                except:
                    content ={"Message":"Error"}
                    return http.request.make_response(json(content), [
                        ('Access-Control-Allow-Origin', '*')])
                return http.request.make_response(json(content), [
                    ('Access-Control-Allow-Origin', '*')])


    @http.route('/api/my_sms', type='http', auth='none', methods=['POST'], csrf=False)
    def class_teacher(self, **kwa):
        content = {}
        if kwa.get('token'):
            check_token = http.request.env['res.users'].search([(
                'token', '=', kwa.get('token'))])
            if check_token:
                try:
                    uid_ = check_token.id
                    classs = http.request.env['mail.channel'].sudo(
                        uid_).search([])

                    if classs:
                        for s in classs :
                            content[s['name']] = {
                                'key' : s.id,
                                'school_id': s.school_id.name,
                                'group_of_class_id': s.group_of_class_id.name,
                                'sehoot_year_id': s.sehoot_year_id.name,
                                'student_numbers': s.student_numbers,
                                'class_room_number': s.class_room_number,
                            }
                except:
                    content ={"Message":"Error"}
                    return http.request.make_response(json(content), [
                        ('Access-Control-Allow-Origin', '*')])
                return http.request.make_response(json(content), [
                    ('Access-Control-Allow-Origin', '*')])

    # @http.route('/api/get_information', type='http', auth='none', methods=['POST'], csrf=False)
    # def get_information(self, **kwa):
    #     content = {}
    #     if kwa.get('token'):
    #         check_token = http.request.env['res.users'].search([(
    #             'token', '=', kwa.get('token'))])
    #         if check_token:
    #             uid_ = check_token.id
    #             if uid_ == SUPERUSER_ID:
    #                 class_inf = http.request.env['bms.study.class'].sudo(
    #                 uid_).search([])
    #                 dict ={
    #                     " Tên Lớp ": class_inf.name,
    #                     "Trường": class_inf.school_id.name,
    #                     " Khối lớp ":class_inf.group_of_class_id.name,
    #                     "Năm học ":class_inf.sehoot_year_id.name,
    #                     "Giáo viên chủ nhiệm": class_inf.homeroom_teacher.name,
    #                     "Phòng học": class_inf.class_room_number,
    #                 }
    #             return http.request.make_response(json(dict), [
    #                 ('Access-Control-Allow-Origin', '*')])




    @http.route('/api/student/create', type='http', auth='none', methods=['POST'], csrf=False)
    def create_student(self, **kw):
        if kw.get('token'):
            check_token = http.request.env['res.users'].search([(
                'token', '=', kw.get('token'))])
            if check_token:
                uid_ = check_token.id
                check_scode = http.request.env['bms.student.management'].sudo(
                    uid_).search([('student_code', '=', kw.get('scode'))])
                if check_scode:
                    return http.request.make_response(
                        json({'Status': 'False',
                         'Message': 'Can not create student because student code exist'}),
                        [('Access-Control-Allow-Origin', '*')])
                try:
                    res = http.request.env['bms.student.management'].sudo(
                        uid_).create({'name': kw.get('name'), 'student_code': kw.get('scode')})
                except:
                    return http.request.make_response(json({
                        'Status': 403, 'Permission': 'Denied'}),
                        [('Access-Control-Allow-Origin', '*')])
                res_json = json({'Status': 200, 'Create': 'Success',
                                 'Sid': res.id, 'Student': res.name or '',
                                 'Student_code': kw['scode']})
                return http.request.make_response(
                    res_json, [('Access-Control-Allow-Origin', '*')])\

    @http.route('/api/student/del', type='http', auth='none', methods=['POST'], csrf=False)
    def del_student(self, **kw):
        if kw.get('token'):
            check_token = http.request.env['res.users'].search([(
                'token', '=', kw.get('token'))])
            if check_token:
                uid_ = check_token.id
                check_scode = http.request.env['bms.student.management'].sudo(
                    uid_).search([('student_code', '=', kw.get('scode'))])
                user = http.request.env['res.users'].search([('login', '=', kw.get('scode'))])
                if not check_scode:
                    return http.request.make_response(json(
                        {'Status': 'False', 'Message': 'Can not delete student because student not exist'}),
                        [('Access-Control-Allow-Origin', '*')])
                if uid_ != SUPERUSER_ID:
                    return http.request.make_response(json({
                        'Status': 403, 'Permission': 'Denied'}),
                        [('Access-Control-Allow-Origin', '*')])
                res = check_scode.sudo().unlink()
                res_user = user.sudo().unlink()
                res_json = json({'Status': 200, 'Deletion': 'Success','Student_code': kw['scode']})
                return http.request.make_response(
                    res_json, [('Access-Control-Allow-Origin', '*')])

    @http.route('/api/student/edit/<id>', type='http', auth='none', methods=['POST'], csrf=False)
    def edit_student(self, **kw):
        pass

    @http.route('/api/deltoken', type='http', auth='user', methods=['POST'], csrf=False)
    def del_token(self, **kw):
        session = http.request.session
        user_id = session['uid']
        user = http.request.env['res.users'].browse(user_id)
        tok = user.token
        if kw.has_key('token'):
            user_token = http.request.env['res.users'].search([('token', '=', kw['token'])])
            if tok == kw['token']: # or user_id == SUPERUSER_ID:
                res = user.sudo().write({'token': False})
                return http.request.make_response(json({
                    'Deletion': 'Success',
                    'Token': tok,
                }))
            else:
                try:
                    res = http.request.env['res.users'].browse(
                        user_token.id).sudo(user_id).write({'token': False})
                except:
                    return http.request.make_response(json({
                        'Deletion': 'False',
                        'Permission': 'Denied',
                        }))
                return http.request.make_response(json({
                    'Deletion': 'Success',
                    'Token': kw['token'],
                }))

    @http.route('/api/retoken', type='http', auth='user', methods=['POST'], csrf=False)
    def renew_token(self, **kw):
        if kw.has_key('token'):
            session = http.request.session
            user = http.request.env['res.users'].browse(session['uid'])
            user_token = http.request.env['res.users'].search(
                [('token', '=', kw['token'])])
            if user.token == kw['token'] or session['uid'] == SUPERUSER_ID:
                new_token = self.get_token(user_token.id)
                res = user_token.sudo().write({'token': new_token})
                return http.request.make_response(json({
                    'Message': 'Renew token',
                    'Token': new_token,
                }))
            return http.request.make_response(json({
                'Message': 'Permission deined',
            }))
        return http.request.make_response(json({
                    'Message': 'Renew token fail',
                }))

    @http.route('/api/teacher/create', type='http', auth='none', methods=['POST'], csrf=False)
    def create_teacher(self, **kw):
        if kw.has_key('token') and kw.has_key('name') and kw.has_key('email'):
            user_id = http.request.env['res.users'].search([('token', '=', kw['token'])])
            try:
                res = http.request.env['hr.employee'].sudo(user_id.id).create({
                    'name': kw['name'],
                    'work_email': kw['email'],})
            except:
                return http.request.make_response(json({
                    'Message': 'Permission deined',
                }))
            return http.request.make_response(json({
                    'Message': 'Create teacher success',
                    'name': kw['name'],
                    'teacher_id': res.id or '',
                }))

    @http.route('/api/teacher', type='http', auth='none', methods=['POST'], csrf=False)
    def get_student_by_hometeacher(self, **kw):
        contx = {}
        if not kw.has_key('token'):
            return http.request.make_response(json({
                'Message': u'Cần nhập vào token'}))
        user_id = http.request.env['res.users'].search([('token', '=', kw['token'])])
        teachers = http.request.env['hr.employee'].sudo().search([])
        if not user_id:
            http.request.make_response(json({
                'Message': u'Token không chính xác. Vui lòng kiểm tra lại'}))
        classs = http.request.env['bms.study.class'].sudo(user_id.id).search([])
        if not teachers:
            http.request.make_response(json({
                'Message': u'Không có giáo viên'}))
        for teacher in teachers:
            if teacher.user_id.id == user_id.id:
                user_teacher = teacher.id
        for cls in classs:
            if cls.homeroom_teacher.id == user_teacher:
                if cls.student_ids:
                    res = {}
                    for std in cls.student_ids:
                        res[std.id] = {'name': std.name,
                                         'gender': std.gender,
                                         'key': std.id,
                                         'student_code': std.student_code,
                                         'gender': std.gender,
                                         'birthday': std.birth_day,
                                         'class': std.study_class_id.name,
                                         'home_teacher': std.study_class_id.homeroom_teacher.name,
                                         'parent': std.parent_id.name
                                       }
                    contx.update({cls.id: res})
        return http.request.make_response(json(contx))

    @http.route('/api/class/<id>', type='http', auth='none', methods=['POST'], csrf=False)
    def get_class(self, **kw):
        if kw.has_key('id') and kw.has_key('token'):
            user = http.request.env['res.users'].search([('token', '=', kw['token'])])
            if user:
                class_ = http.request.env['bms.study.class'].sudo(user.id).search([('id', '=', kw['id'])])
                if class_:
                    contx = {'Message': 'Class infomation', 'id': class_.id,
                             'class': class_.name}
                    student_ = {}
                    for student in class_.student_ids:
                        student_.update({
                            student.id: student.name or ''})
                    contx['students'] = student_
                    return http.request.make_response(json(contx),
                                                  [('Access-Control-Allow-Origin', '*')])
                return http.request.make_response(
                    json({'Message': 'No permission or class id invalid! Check again'}),
                    [('Access-Control-Allow-Origin', '*')])
            return http.request.make_response(json({'Message': 'Token invalid'}),
                                              [('Access-Control-Allow-Origin', '*')])
        return http.request.make_response(json({
                    'Message': 'Have not id or token. Please check again!',
                }))

    @http.route('/api/school/<id>', type='http', auth='none', methods=['POST'], csrf=False)
    def get_all_class(self, **kw):
        if kw.has_key('id') and kw.has_key('token'):
            user = http.request.env['res.users'].search(
                [('token', '=', kw['token'])])
            if user:
                all_class = http.request.env['bms.study.class'].sudo().search(
                    [('active', '=', True),('school_id', '=', int(kw['id']))])
                if all_class:
                    contx = {}
                    for school in user.company_ids:
                        class_ = {}
                        for cls in all_class:
                            if school.id == cls.school_id.id:
                                class_[cls.id] = {
                                    'class_name': cls.name,
                                    'home_teacher': cls.homeroom_teacher.sudo().name or '',
                                    'home_teacher_id': cls.homeroom_teacher.id or '',}
                        contx.update({
                            school.id : {school.name: class_}
                        })
                    return http.request.make_response(json(contx))
                return http.request.make_response(json(
                    {'Message': 'Have not school'}))
            return http.request.make_response(json(
                {'Message': 'Your token or id school invalid'}
            ))
        return http.request.make_response(json(
            {'Message': 'Please input school id and token'}))

    @http.route('/api/groupsms', type='http', auth='none', methods=['POST'], csrf=False)
    def get_group_student(self, **kw):
        contx = {}
        if not kw.has_key('token'):
            return http.request.make_response(json({
                'Message': 'Not have token key. Please check again!'}))
        user_id = http.request.env['res.users'].search([('token', '=', kw['token'])])
        if not user_id:
            return http.request.make_response(json({
                'Message': 'Token invalid'}))
        try:
            group_students = http.request.env['group.student'].sudo(user_id.id).search([])
        except:
            return http.request.make_response(json({
                'Message': 'You not have permission or not have group student'}))
        for group in group_students:
            if user_id.company_id.id == group.school_id.id:
                contx[group.id] = {
                'key':group.id or '',
                'group': group.name or '',
                'class': group.study_class_id.name or '',
                'student_numbers': group.student_numbers or '',
                'student_ids': group.student_ids.ids or ''}
        return http.request.make_response(json(contx))

    @http.route('/api/sms/create_all', type='http', auth='none', methods=['POST'], csrf=False)
    def create_sms(self, **kw):
        if kw.has_key('token') and kw.has_key('name') and kw.has_key('school_id') and kw.has_key(
                'sehoot_year_id') and kw.has_key('study_class_id') and kw.has_key('date_sending') and kw.has_key('message_no_accent'):
            user_id = http.request.env['res.users'].search([('token', '=', kw['token'])])
            res1 = http.request.env['sms.management'].sudo(user_id.id).search([('id', '=', 2)])
            print(res1.date_sending)
            try:
                res = http.request.env['sms.management'].sudo(user_id.id).create({
                    'name': kw['name'],
                    'school_id': kw['school_id'],
                    'sehoot_year_id': kw['sehoot_year_id'],
                    'study_class_id': kw['study_class_id'],
                    'date_sending': kw['date_sending'],
                    'is_send_all': True,
                    'message_no_accent': kw['message_no_accent'],
                    'accent_vietnamese': 'no_accent',
                })
                res._send_all_change()
                res.action_send()
            except:
                return http.request.make_response(json({
                    'Message': 'Permission deined',
                }))
            return http.request.make_response(json({
                'Message': 'Create sms success',
                'name': kw['name'],
            }))

    @http.route('/api/sms/create_t', type='http', auth='none', methods=['POST'], csrf=False)
    def create_teacher(self, **kw):
        if kw.has_key('token') and kw.has_key('name') and kw.has_key(
                'school_id') and kw.has_key('sehoot_year_id') and kw.has_key('study_class_id') and kw.has_key(
            'date_sending') and kw.has_key('message_no_accent'):
            id = kw['group_student_ids'].split(',')
            id = map(lambda x: int(x),id)
            # id = kw['group_student_ids'].encode('ascii', 'ignore')
            thamsogroup = http.request.env['group.student'].browse(id)
            print("*" * 30)
            user_id = http.request.env['res.users'].search([('token', '=', kw['token'])])
            try:

                request = http.request.env['sms.management'].sudo(user_id.id).create({
                    'name': kw['name'],
                    'school_id': kw['school_id'],
                    'sehoot_year_id': kw['sehoot_year_id'],
                    'study_class_id': kw['study_class_id'],
                    'date_sending': kw['date_sending'],
                    'message_no_accent': kw['message_no_accent'],
                    'group_student_ids': (1,2),
                    'accent_vietnamese': 'no_accent',
                    'is_send_all': False,
                })
                print("*"*30)
                print(kw['group_student_ids'])
                print(request.name)
                request.group_student_ids = thamsogroup
                request._send_group_change()
                request.action_send()

            except:
                return http.request.make_response(json({
                    'Message': 'Lỗi không gửi được',
                }))
            return http.request.make_response(json({
                'Message': 'Gửi thành công',
            }))

    @http.route('/api/sms/create_one', type='http', auth='none', methods=['POST'], csrf=False)
    def create_one(self, **kw):
        if kw.has_key('token') and kw.has_key('name') and kw.has_key(
                'school_id') and kw.has_key('sehoot_year_id') and kw.has_key('study_class_id') and kw.has_key(
            'date_sending') and kw.has_key('message_no_accent') and kw.has_key('student_ids') :
            id = kw['student_ids'].split(',')
            id = map(lambda x: int(x), id)
            # id = kw['student_ids'].encode('ascii', 'ignore')
            print(id)
            thamsostudent = http.request.env['bms.student.management'].browse(id)
            user_id = http.request.env['res.users'].search([('token', '=', kw['token'])])
            try:

                request = http.request.env['sms.management'].sudo(user_id.id).create({
                    'name': kw['name'],
                    'school_id': kw['school_id'],
                    'sehoot_year_id': kw['sehoot_year_id'],
                    'study_class_id': kw['study_class_id'],
                    'date_sending': kw['date_sending'],
                    'message_no_accent': kw['message_no_accent'],
                    'is_send_all': False,
                    'accent_vietnamese': kw['accent_vietnamese'],
                    'student_ids': kw['student_ids'],
                })
                request.student_ids = thamsostudent
                request.action_send()

            except:
                return http.request.make_response(json({
                    'Message': 'Lỗi không gửi được',
                }))
            return http.request.make_response(json({
                'Message': 'Gửi thành công',
            }))

    @http.route('/api/sms/get_images', type='http', auth='none', methods=['POST'], csrf=False)
    def get_images(self, **kw):
            content ={}
            user = http.request.env['res.users'].search([('token', '=', kw['token'])])
            id = kw['study_class_id'].split(',')
            id = map(lambda x: int(x), id)
            if user:
                try:
                    getimage = http.request.env['upload.images'].sudo().search([('study_class_id', '=', id)])
                    print(getimage)
                    for s in getimage:
                        content[s['id']] = {
                            'name': s.name,
                            'URL Images': s.imageURL,
                    }
                except:
                    content ={}
                    return http.request.make_response(json(content), [
                        ('Access-Control-Allow-Origin', '*')])
            return http.request.make_response(json(content), [
                ('Access-Control-Allow-Origin', '*')])

    @http.route('/api/sms/history', type='http', auth='none', methods=['POST'], csrf=False)
    def sms_history(self, **kw):
        content = {}
        user = http.request.env['res.users'].search([('token', '=', kw['token'])])
        try:
            gethistory = http.request.env['sms.management'].sudo().search([])
        # print(gethistory.id)
            if gethistory:
                    print(gethistory)
                    for i in gethistory:
                        print(i)
                        content[i['id']] = {
                        'key':i.id,
                        'message_no_accent': i.message_no_accent,
                        'study_class_id': i.study_class_id.name,
                        'date_sending': i.date_sending,
                        'name': i.name,
                    }

            return http.request.make_response(json(content), [
                ('Access-Control-Allow-Origin', '*')])
        except:
            content = {"Message": "Error"}
            return http.request.make_response(json(content))

    @http.route('/api/sms/create_images', type='http', auth='none', methods=['POST'], csrf=False)
    def create_images(self, **kw):
        if kw.has_key('token') and kw.has_key('name')and kw.has_key('namedatas') and kw.has_key(
                'study_class_id') and kw.has_key('date') and kw.has_key(
            'datas_fname') and kw.has_key('datas'):
            user_id = http.request.env['res.users'].search([('token', '=', kw['token'])])
            try:
                datas = kw['datas'].encode('ascii', 'ignore')
                create_data = http.request.env['ir.attachment'].sudo(user_id.id).create({
                    'name': kw['namedatas'],
                    'datas': datas,
                    'datas_fname': kw['datas_fname'],
                    'res_model': 'res.users',
                    'type': 'binary',
                })
                print(create_data.id)
                id = create_data.id
                # thamsoscreate = http.request.env['ir.attachment'].browse(id)
                # print(thamsoscreate)
                request = http.request.env['upload.images'].sudo(user_id.id).create({
                    'name': kw['name'],
                    'study_class_id': kw['study_class_id'],
                    'attachment_id': id,
                    'date': kw['date'],
                })
                # request.attachment_id = thamsoscreate
                request.create_url()

            except:
                return http.request.make_response(json({
                    'Message': 'Error',
                }))
        return http.request.make_response(json({
                    'Message': 'Done'}))

    @http.route('/api/sms/send_school', type='http', auth='none', methods=['POST'], csrf=False)
    def send_school(self, **kw):
        content = {}
        user = http.request.env['res.users'].search([('token', '=', kw['token'])])
        sendschool = http.request.env['res.company'].sudo(user.id).search([])
        searchclass = http.request.env['bms.study.class'].sudo(user.id).search([])
        # searchstudent = http.request.env['bms.student.management'].sudo().search([('school_id', '=', sendschool.id)])
        searchclass = map(lambda x: int(x), searchclass)
        for i in searchclass:
            request = http.request.env['sms.management'].sudo().create({
                    'name': kw['name'],
                    'school_id': sendschool.id,
                    'sehoot_year_id': kw['sehoot_year_id'],
                    'study_class_id': i,
                    'date_sending': kw['date_sending'],
                    'message_no_accent': kw['message_no_accent'],
                    'is_send_all': True,
                    'accent_vietnamese': 'no_accent',
                    'student_ids': '',
                })
            request._send_all_change()
            request.action_send()
        return http.request.make_response(json({
                'Message': 'Done'
            }), [
                ('Access-Control-Allow-Origin', '*')])

    @http.route('/api/sms/send_teacher', type='http', auth='none', methods=['POST'], csrf=False)
    def send_teacher(self, **kw):
        content = {}
        user = http.request.env['res.users'].search([('token', '=', kw['token'])])
        if kw.has_key('name') and kw.has_key('school_id') and kw.has_key('date_sending'):
            request = http.request.env['principal.sms'].sudo().create({
                'name': kw['name'],
                'school_id': kw['school_id'],
                'date_sending': kw['date_sending'],
                'is_send_all': True,
            })
            request._send_all_change()
            request.action_send()
        return http.request.make_response(json({
            'Message': 'Done'
        }), [
            ('Access-Control-Allow-Origin', '*')])

    @http.route('/api/blog/get_info', type='http', auth='none', methods=['POST'], csrf=False)
    def get_info(self, **kw):
        content = {}
        user = http.request.env['res.users'].search([('token', '=', kw['token'])])
        try:
            domain = [('website_published', '=', False)]
            getblog = http.request.env['blog.post'].sudo(user.id).search(domain)
            ids = getblog.ids
            print(ids)
            for i in ids:
                bloginfo = http.request.env['blog.post'].sudo(user.id).search([('id', '=', i)])
                listtag = ''
                if bloginfo:
                    # xử lý tử 2 tag trở lên
                    tags = bloginfo.tag_ids
                    if len(tags) > 1:
                        for j in tags:
                            print(j.id)
                            blogtag = http.request.env['blog.tag'].sudo().search([('id', '=', j.id)])
                            listtag = listtag + ' , ' + str(blogtag.name)
                    content[i] = {'Blog': bloginfo.blog_id.name or '',
                                  'Name': bloginfo.name or '',
                                  'Subtitle': bloginfo.subtitle or '',
                                  'Tag': listtag or '',
                                  'Author': bloginfo.author_id.name or '',
                                  'Create Date': bloginfo.create_date or '',
                                  'Published Date': bloginfo.post_date or '',
                                  'Last Contributor': bloginfo.write_uid.name or '',
                                  'Last Modified on': bloginfo.write_date or ''}
                    print(content)
            return http.request.make_response(json(content), [
                ('Access-Control-Allow-Origin', '*')])
        except:
            content = {"Message": "Error"}
            return http.request.make_response(json(content))

    @http.route('/api/get_infomation_more', type='http', auth='none', methods=['POST'], csrf=False)
    def get_infomation_more(self, **kw):
        content = {}
        user = http.request.env['res.users'].search([('token', '=', kw['token'])])
        info = http.request.env['more.infomation'].sudo().search([])
        content[info.id] = {
            'website': info.website or '',
            'facebook': info.facebook or '',
            'email': info.email or '',
            'phone': info.phone or '',
            'address': info.address or '',
            'intro': info.intro or '',
                            }
        return http.request.make_response(json(content), [
            ('Access-Control-Allow-Origin', '*')])

    # def test(self, **kw):
    #         user_id = http.request.env['res.users'].search([('token', '=', kw['token'])])
    #         # res = http.request.env['upload.images'].sudo().search([('id','=',11)])
    #         # datas = kw['datas'].encode('ascii', 'ignore')
    #         # list1 =[]
    #         # list1.append(datas)
    #         # print(list1)
    #         id = kw['attachment_id'].split(',')
    #         id = map(lambda x: int(x), id)
    #         print(id)
    #         thamsostudent = http.request.env['ir.attachment'].browse(id)
    #         request = http.request.env['upload.images'].sudo().create({
    #             'name': kw['name'],
    #             'study_class_id': kw['study_class_id'],
    #             'attachment_id': kw['attachment_id'],
    #             'date': kw['date'],
    #         })
    #         request.attachment_id = thamsostudent
    #         # datas = kw['datas'].encode('ascii', 'ignore')
    #         # create_data = http.request.env['ir.attachment'].sudo(user_id.id).create({
    #         #     'name': kw['namedatas'],
    #         #     'datas': datas,
    #         #     'datas_fname': kw['datas_fname'],
    #         #     'res_model': 'res.users',
    #         #     'type': 'binary',
    #         # })
    #         # print(create_data.id)
    #         # datas = kw['datas'].encode('ascii', 'ignore')
    #         # create_data = http.request.env['ir.attachment'].sudo(user_id.id).create({
    #         #     'name': kw['namedatas'],
    #         #     'datas': datas,
    #         #     'datas_fname': kw['datas_fname'],
    #         #     'res_model': 'res.users',
    #         #     'type': 'binary',
    #         # })
    #         # print(create_data.id)
    #         return http.request.make_response(json({
    #             'Message': 'Done',
    #         }))


    @http.route('/api/lay_thong_tin_xe', type='http', auth='none', methods=['POST'], csrf=False)
    def get_all_car_information(self, **kw):
        content = {}
        car_ids = http.request.env['tgl.quanlyxe'].sudo().search([])
        print(car_ids)
        # tgl.quanlyxe(1, 2)
        for car_id in car_ids:
            print(car_id)
            # tgl.quanlyxe(1,)
            print(car_id.name)
            # 29C-12345
            print(car_id.trongluongxe)
            # 20000.0
            car_id.trongluongxe = 30000

            print(car_id.taixe_id)
            taixe = car_id.taixe_id
            tentaixe = car_id.taixe_id.name
            # car_id.taixe_id.name = "Anh Long"
            print(taixe.name)
            # hr.employee(2,)
            content[car_id.id] = {
                'name': car_id.name or '',
                'trongluongxe': car_id.trongluongxe or '',
                'taixe_id': car_id.taixe_id.id or '',
                'ten_tai_xe': car_id.taixe_id.name or ''
            }

        return http.request.make_response(json(content), [
            ('Access-Control-Allow-Origin', '*')])
