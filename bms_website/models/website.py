# -*- coding: utf-8 -*-
# Copyright (C) 2006 Serenco JSC (<http://serenco.net>).
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

from odoo import api, models
from odoo.addons.website.models.website import slug

class Website(models.Model):
    _inherit = 'website'

    @api.model
    def get_blog_data(self):
        limit = 7
        # blog_blog_1 = self.sudo().env.ref('website_blog.blog_blog_1')
        blog_blog_1 = self.env['blog.blog'].browse(1)
        print(blog_blog_1)
        post_domain = [('blog_id', '=', blog_blog_1.id)]
        post_s = self.env['blog.post'].search(
            post_domain, limit=limit)

        base_url = '/blog/our-blog-{}/post/'.format(
            slug(blog_blog_1),
        )
        res = {}
        for post in post_s:
            name = post.name or 'Our post'
            url = base_url + slug(post)
            res[post.id] = (name, url)

        print(res)

        return res
