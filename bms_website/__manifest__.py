# -*- coding: utf-8 -*-
# Copyright (C) 2006 BMS Group Global (<http://www.bmsgroupglobal.com>).
# License AGPL-3.0 or later (http://www.gnu.org/licenses/agpl).

{
    'name': 'BMS Website',
    'version': '10.0.1.0.0',
    'author': 'BMS Group Global',
    'category': 'BMS Modules',
    'license': 'AGPL-3',
    'website': 'http://www.bmsgroupglobal.com',
    'depends': [
        'base',
        'website',
        'website_blog',
    ],
    'data': [
        'templates/homepage_template.xml',
    ],
    'demo': [
    ],
    'css': [
        'bms_website/static/src/css/bms.css',
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
}
