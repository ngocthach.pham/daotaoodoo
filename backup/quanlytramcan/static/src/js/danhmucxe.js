odoo.define('quanlytramcan.snippets_animation', function(require) {
  "use strict";
  var animation = require('web_editor.snippets.animation'),
    Model = require('web.Model');
  animation.registry.truck_snippet = animation.Class.extend({
    selector: ".truck_snippet",
    start: function() {
      var self = this,
        number = 3;
      _.each(this.$el.attr('class').split(/\s+/), function(cls) {
        if (cls.indexOf('truck_snippet-show') == 0) {
          number = parseInt(cls.substring('truck_snippet-show'.length));
        }
      });
      this.$el.find('td').parents('tr').remove();
      new Model('tgl.quanlyxe').call('search_read', [], {
        domain: [],
        fields: [
          'name', 'trongluongxe'
        ],
        order: 'trongluongxe desc',
        limit: number
      }).then(function(data) {
        console.log(data);
        var $table = self.$el.find('table');
        _.each(data, function(xe) {
          $table.append(jQuery('<tr />').append(jQuery('<td />').text(xe.name), jQuery('<td />').text(xe.trongluongxe)));
        })
      });
    }
  });
});
