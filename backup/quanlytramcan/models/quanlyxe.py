# -*- coding: utf-8 -*-

from odoo import models, fields, api


class DanhMucXe(models.Model):
    _name = 'tgl.quanlyxe'

    name = fields.Char(string="Biển số xe", required=True)
    trongluongxe = fields.Float(string="Trọng lượng xe rỗng",  required=True)
    taixe_id = fields.Many2one("hr.employee", string="Chọn tài xế")
