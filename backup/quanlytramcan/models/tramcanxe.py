# -*- coding: utf-8 -*-

from odoo import models, fields, api


class TramCanXe(models.Model):
    _name = "tgl.tramcanxe"

    thoigian = fields.Datetime(string="Thời Gian", required=True)
    vaora = fields.Boolean(string="Vào Ra")
    trongluong = fields.Float(string="Trọng Lượng Cân",  required=True)
    id_xe = fields.Many2one(comodel_name="tgl.quanlyxe", string="Xe",
                            required=True, ondelete='cascade')
