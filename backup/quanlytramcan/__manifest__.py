# -*- coding: utf-8 -*-
{
    'name': "Quản lý trạm cân",

    'summary': """
        Module quản lý trạm cân""",

    'description': """
        Long description of module's purpose
    """,

    'author': "My Company",
    'website': "http://www.yourcompany.com",
    #support for show when filter App
    'installable': True,
    'application': True,
    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'BMS Application',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['base', 'hr', 'crm'],

    # always loaded
    'data': [
        # 'security/ir.model.access.csv',
        'views/lenhxuatxe.xml',
        'views/quanlyxe.xml',
        'views/tramcanxe.xml',
        'views/snippets.xml',
        'views/menu.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
        #'demo/demo.xml',
    ],
}
