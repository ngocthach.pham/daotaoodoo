# -*- coding: utf-8 -*-

from odoo import models, fields


class SaigonRoomCategory(models.Model):
    _name = 'saigon.room.category'
    _description = "Room Category"

    name = fields.Char('Room Category', required=True)
    code_room_category = fields.Char('Code')
    number_adults = fields.Integer('Number adults')
    number_children = fields.Integer('Number children')
    description = fields.Html("Description")

    sum = fields.Integer('count')
    room_id = fields.One2many('saigon.room', 'id')

    def sum(self):
        self.sum = range(10,90, 3)

