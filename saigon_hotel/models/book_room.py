# -*- coding: utf-8 -*-

from odoo import api, models, fields, _
from odoo.exceptions import UserError
from datetime import datetime
import logging

_logger = logging.getLogger(__name__)

class SaigonBookRoom(models.Model):
    _name = 'saigon.book.room'

    name = fields.Char('Name', required=True)
    type_book = fields.Selection([('company','Company'),('person','Person')], default="ps", string="Type", required=True)
    customer_id = fields.Many2one('res.partner', string="Customer")
    room_id = fields.Many2one('saigon.room', string="Room")
    room_category_id = fields.Many2one('saigon.room.category', string="Room Category")
    number_adults = fields.Integer(string="Number of adults")
    number_children = fields.Integer("Number of children")
    check_in = fields.Datetime("Check in")
    check_out = fields.Datetime("Check out")
    state = fields.Selection([('cr','Create'),('rv','Reserved'),('ci','Check in'), ('cu','Check out'), ('cl','Close'), ('cc','Cancel')], default="cr", required=True)
    total_price = fields.Float("Total price")
    # Price List fields
    price_list_id = fields.Many2many('saigon.price.policy', string="Price List")

    description = fields.Text("Description")

    @api.onchange('room_id')
    def _onchange_room_id(self):
        if self.room_id.category_room:
            self.room_category_id = self.room_id.category_room.id
        return

    @api.onchange('customer_id')
    def _onchange_customer_id(self):
        self.type_book = self.customer_id.company_type
        return

    # @api.onchange('type_book')
    # def _onchange_type_book_id(self):
    #     return {'domain': {'customer_id': [('company_type', '=', self.type_book)]}}

    @api.model
    def create(self, vals):
        # _logger.warning(vals)
        if 'check_in' in vals and 'check_out' in vals:
            check_in = datetime.strptime(vals['check_in'], '%Y-%m-%d %H:%M:%S')
            if check_in > datetime.now():
                raise UserError(_('Check in can not greater than current day: %s' % datetime.now()))
            if check_in > datetime.strptime(vals['check_out'], '%Y-%m-%d %H:%M:%S'):
                raise UserError(_('Check in can not greater check out: %s') % vals['check_out'])
        else:
            raise UserError(_('Check in or check out not null'))

        season_lines = self._getSeason(vals['check_in'], vals['check_out'])
        # _logger.warning(season_lines);
        season = []
        total_price = 0
        for season_line in season_lines:
            if datetime.strptime(season_line.start_time,
                                 '%Y-%m-%d %H:%M:%S') < datetime.now() < datetime.strptime(
                season_line.end_time, '%Y-%m-%d %H:%M:%S'):
                season = season_line
                break
        if season:
            policy = self.env['saigon.price.policy'].search([('season_id', '=', season.id)])
            print(policy)
            if policy:
                policy_lines = self.env['saigon.price.policy.line'].search([('policy_id', '=', policy[0].id)])
                if policy_lines:
                    price_first_hour = price_next_hour = price_over_hour = 0
                    for policy_line in policy_lines:
                        if policy_line.room_category_id.id == vals[
                            'room_category_id'] and policy_line.price_type == 'fh':
                            price_first_hour = policy_line.price
                        if policy_line.room_category_id.id == vals[
                            'room_category_id'] and policy_line.price_type == 'nh':
                            price_next_hour = policy_line.price
                        if policy_line.room_category_id.id == vals[
                            'room_category_id'] and policy_line.price_type == 'ov':
                            price_over_hour = policy_line.price
                    time = datetime.strptime(vals['check_out'], '%Y-%m-%d %H:%M:%S') - datetime.strptime(
                        vals['check_in'], '%Y-%m-%d %H:%M:%S')
                    hour = minutes = day = 0

                    hour = str(time)[-7:-6]
                    minutes = str(time)[-5:-3]
                    day = str(time)[-15:-14]

                    if minutes != '' and int(minutes) > 10:
                        hour = int(hour) + 1
                    if hour != '' and int(hour) > 1:
                        total_price = price_first_hour + (int(hour) - 1) * price_next_hour
                        if day != '' and int(day) > 0:
                            total_price = total_price + (int(day) * price_over_hour)
                    else:
                        total_price = price_next_hour
                        if day != '' and int(day) > 0:
                            total_price = total_price + (int(day) * price_over_hour)

        # raise UserError(_('total_price %s' %total_price))
        vals['total_price'] = total_price
        res = super(SaigonBookRoom, self).create(vals)
        return  res

    @api.multi
    def write(self, vals):
        # _logger.warning(vals)
        if not 'check_in' in vals:
            vals['check_in'] = self.check_in
        if not 'check_out' in vals:
            vals['check_out'] = self.check_out
        if not 'room_category_id' in vals:
            vals['room_category_id'] = self.room_category_id.id

        check_in = datetime.strptime(vals['check_in'], '%Y-%m-%d %H:%M:%S')
        if check_in > datetime.now():
            raise UserError(_('Check in can not greater than current day: %s' % datetime.now()))
        if check_in > datetime.strptime(vals['check_out'], '%Y-%m-%d %H:%M:%S'):
            raise UserError(_('Check in can not greater check out: %s') % vals['check_out'])

        season_lines = self._getSeason(vals['check_in'], vals['check_out'])
        # _logger.warning(season_lines);
        season = []
        total_price = 0
        for season_line in season_lines:
            if datetime.strptime(season_line.start_time,
                                 '%Y-%m-%d %H:%M:%S') < datetime.now() < datetime.strptime(
                    season_line.end_time, '%Y-%m-%d %H:%M:%S'):
                season = season_line
                break
        if season:
            policy = self.env['saigon.price.policy'].search([('season_id', '=', season.id)])
            print(policy)
            if policy:
                policy_lines = self.env['saigon.price.policy.line'].search([('policy_id', '=', policy[0].id)])
                if policy_lines:
                    price_first_hour = price_next_hour = price_over_hour = 0
                    for policy_line in policy_lines:
                        if policy_line.room_category_id.id == vals[
                            'room_category_id'] and policy_line.price_type == 'fh':
                            price_first_hour = policy_line.price
                        if policy_line.room_category_id.id == vals[
                            'room_category_id'] and policy_line.price_type == 'nh':
                            price_next_hour = policy_line.price
                        if policy_line.room_category_id.id == vals[
                            'room_category_id'] and policy_line.price_type == 'ov':
                            price_over_hour = policy_line.price
                    time = datetime.strptime(vals['check_out'], '%Y-%m-%d %H:%M:%S') - datetime.strptime(
                        vals['check_in'], '%Y-%m-%d %H:%M:%S')
                    hour = minutes = day = 0

                    hour = str(time)[-7:-6]
                    minutes = str(time)[-5:-3]
                    day = str(time)[-15:-14]

                    if minutes != '' and int(minutes) > 10:
                        hour = int(hour) + 1
                    if hour != '' and int(hour) > 1:
                        total_price = price_first_hour + (int(hour) - 1) * price_next_hour
                        if day != '' and int(day) > 0:
                            total_price = total_price + (int(day) * price_over_hour)
                    else:
                        total_price = price_next_hour
                        if day != '' and int(day) > 0:
                            total_price = total_price + (int(day) * price_over_hour)

        # raise UserError(_('total_price %s' %total_price))
        vals['total_price'] = total_price
        res = super(SaigonBookRoom, self).write(vals)
        return res


    def _getSeason(self, start, end):
        # lis = self.env['saigon.season'].search([('start_time', '>', end)],[('end_time', '<', start)])
        lis = self.env['saigon.season'].search([])
        return lis