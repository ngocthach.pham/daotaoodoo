# -*- coding: utf-8 -*-

from odoo import models, fields

import logging

_logger = logging.getLogger(__name__)

# class SaigonPriceType(models.Model):
#     _name = 'saigon.price.type'
#     _description = 'Price type'
#
#     name = fields.Char('Name', required=True)
#     description = fields.Text(string='Description')

class SaigonPricePolicyLine(models.Model):
    _name = 'saigon.price.policy.line'
    _description = 'Price type'

    name = fields.Char('Name', required=True)
    room_category_id = fields.Many2one('saigon.room.category', string='Room category', required=True)
    price = fields.Float('Price', default='10000', required=True)
    # price_type = fields.Many2one('saigon.price.type', string="Price type", required=True)
    price_type = fields.Selection([('fh', 'First Hour'),('nh','Next Hour'),('ov','Over Night')], default="ov", required=True)
    policy_id = fields.Many2one('saigon.price.policy', required=True)
    description = fields.Text(string='Description')

class SaigonPricePolicy(models.Model):
    _name = 'saigon.price.policy'
    _description = 'Price season'

    name = fields.Char('Name', required=True)
    policy_line_ids = fields.One2many('saigon.price.policy.line',
        'policy_id', string="Policy line")
    season_id = fields.Many2one('saigon.season', string="Session")
    description = fields.Text(string='Description')

class SaigonSeason(models.Model):
    _name = 'saigon.season'
    _description = 'Session'

    name = fields.Char('Name', required=True)
    start_time = fields.Datetime(string='Start time', required=True)
    end_time = fields.Datetime(string='End time', required=True)
    policy_season_ids = fields.One2many('saigon.price.policy', 'season_id', string='Policy')
    description = fields.Text(string='Description')
