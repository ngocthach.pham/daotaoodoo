# -*- coding: utf-8 -*-

from odoo import models, fields

import logging

_logger = logging.getLogger(__name__)

class SaigonRoom(models.Model):
    _name = 'saigon.room'

    name = fields.Char('Name', required=True)
    category_room = fields.Many2one('saigon.room.category', string="Category")
    max_capacity = fields.Integer('Số người tối đa ', default='1')
    is_vip = fields.Boolean(string='Phòng Vip',default=False)
    start_time = fields.Date(string='Ngày đi vào hoạt động')
    discount = fields.Float(string="Discount")
    saigon_product_line_ids = fields.One2many('saigon.product.line', 'saigon_room_id', string='Product Line')
    image = fields.Many2many('ir.attachment', string='Atttach file')


class SaigoProductLine(models.Model):
    _name = 'saigon.product.line'
    _description = 'Product Line'

    product_id = fields.Many2one('product.product', string='Product')
    number = fields.Integer('Number', default='1',)
    saigon_room_id = fields.Many2one('saigon.room', ondelete='CASCADE', string='Room')
