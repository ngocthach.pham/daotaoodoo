# -*- coding: utf-8 -*-
{
    'name': 'SaiGon - Hotel Management',
    'summary': 'Hotel Management',
    'version': '10.0.1.0.0',
    'author': 'BMS Group Global',
    'website': 'http://bmsgroupglobal.com/',
    'depends': [
        'base','product'
    ],
    'data': [
        'security/saigon_rule.xml',
        'security/ir.model.access.csv',
        'views/saigon_room_view.xml',
        'views/saigon_room_category_view.xml',
        'views/saigon_season_view.xml',
        'views/saigon_price_policy_view.xml',
        'views/saigon_book_room_view.xml',
        'reports\saigon_price_policy_report.xml',
	    'views/menu_view.xml'

    ],
    'demo': [],
    'installable': True,
    'application': True,
    'license': 'AGPL-3',
}
