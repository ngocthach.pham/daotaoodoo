# -*- coding: utf-8 -*-
from odoo import http

class Quanlytramcan(http.Controller):

    @http.route('/danh-sach-xe/', auth='public', website=True)
    def index(self, **kw):
        Danhsachxe = http.request.env['tgl.quanlyxe']
        return http.request.render('quanlytramcan.DanhSachXe', {
            'Danhsachxe': Danhsachxe.sudo().search([])
        })

    @http.route('/chi-tiet-xe/<int:id>/', auth='public', website=True)
    def indexId(self, id):
        Chitietxe = http.request.env['tgl.quanlyxe']
        print("--------------------------------------")
        return http.request.render('quanlytramcan.ChiTietXeId', {
            'Chitietxe': Chitietxe.search([
                ('id', '=', id),
            ])
        })
